/**
  ******************************************************************************
  * @file      main.c
  * @brief      
  *            This module performs:
  *                - main function takes input from command line
  *                - Send it to cmdParser module
  *                - openAPICmdParserToMotorEmu function send the data to motorEmu module
  *                - openAPIMotorEmuToCmdParser takes the response from motorEmu module
  
  ******************************************************************************
**/

#include <stdio.h>
#include <stdlib.h>
#include "main.h"
#include "cmdParser.h"
#include "motorEmu.h"

/**
  * @brief A dummy function that transfer data from cmdParser to motor emulator
  			Also process the received data as motor emulator
  * @param ch: data bytes from command parser
  * @param len: data length
*/
void openAPICmdParserToMotorEmu(char *ch, int len) //TX to motor emulator function will come here
{	int i=0;
	for( i = 0 ; i < len ; i++)
	{	FillMotorEmuRxBuf(ch[i]);	//this function fills buffer for Rx module
	} 
	ProcessMotorEmu();
}

/**
  * @brief A dummy function that received response data from motor emulator to cmdParser
  * @param ch: data bytes from motor emulator
  * @param len: data length
*/

void openAPIMotorEmuToCmdParser(char *ch, int len)//RX from motor emu function will come here
{	int i=0;
	for( i = 0 ; i < len ; i++)
	{	FillCmdParserRxBuf(ch[i]);//This function fills buffer for rx of Cmd Parser
	} 
} 

/**
  * @brief Init both motor emulator and cmdParser module
           Takes data from from command line
           Also print current motor state
*/
int main(int argc, char* argv[]) 
{  	CmdParserInit();	//Initialize Tx module dependencies
	MotorEmuInit();	//Initialize Rx(motor side) module dependencies
	if( argc == 2 ) //Rx command from command line	
	{	ProcessCmdParser(argv[1]);	//process the module command and send it to motor side
   	}
   	else
   	{		
   		printf("Warning : Please pass a valid command\n");
	}
	PrintMotorCurrentState();	//print motor current state
	return 0;
}
