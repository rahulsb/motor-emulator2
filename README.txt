# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
A simulator of a communication protocol with an electric motor. This is aconsole application, which accept motor commands
from user input and from batch file, parse the command, send it to the motor, get response from the motor and report the 
result to the console.
Command parser and motor emulator are separate modules in the project.

The program consistes of following files:
	main.c 				main.h	//program main takes I/P from command line and manage API function
	motion.txt					//saves command sequence
Command parser files:
	cmdParser.c 		cmdParser.h			//manages command input from command line and received data from motor
	cmdParserLogFile.c 	cmdParserLogFile.h	//saves log of command
	cmdParserTimer.c	cmdParserTimer.h	//manages timeout for commands(if motor emulator not responsing then gives timeout)
	logFile.txt
	
Motor Emulator files:
	motorEmu.c 			motorEmu.h			//Takes command from command parser, understand it and respond back
	motorEmuFiles.c 	motorEmuFiles.h 	//manages files like profiles and current settings of motor
	profile.txt
	saveCurrent.bin

Program flow summary:
		Command parser					||				Motor Emulator
			|					||		
		ProcessCmdParser				||
			|					||		
		CommandAssigner					||
			|					||		
		ProcessObject					||
			|					||		
		SendCommand---------->openAPICmdParserToMotorEmu---->ProcessMotorEmu
								||		|
								||	ProcessRxData
								||		|
		CPReadResponse<-----openAPIMotorEmuToCmdParser<-------SendResponse


### How do set up? ###
Tested on Dev C++(v5.11) in windows10-64bit and gcc in ubuntu-16.04.6-64bit
For windows:
	1.In DevC++ /Execute/Compile & Run/
	2.Then open command prompt
	3.change directory to program file using:: cd File Location here
	4. run using:: ./motor "some valid command here"
	
For Linux:
	1.Then open command prompt
	2.change directory to program file using:: cd File Location here
	3.type:: make clean
	4.type:: make
	5. run using:: ./motor "some valid command here"
### Valid Commands ###
-----------------Commands															 Description
	set profile <profile number> <position in mm> <velocity in mm/s>		Sets the parameters of the profile	with specified number
	set immediate <position in mm> <velocity in mm/s> 						Configures immediate position and velocity
	start profile <profile number>											configures motor to profile mode and starts motion to profile with specified number
	start immediate 														configures motor to immediate mode starts motion to configured immediate position with	immediate velocity
	stop 																	stops the motor
	get profile <profile number> 											reads profile position and velocity
	get mode 																reads operation mode
	get error 																reads error code
	get status 																reads status word
	reset 																	clears error code and sets motor to immediate mode

### Contribution guidelines ###
Some example tests:
For windows:
	motor.exe motion.txt
	motor.exe "set profile 1 100 2"
	motor.exe "start profile 1"
	motor.exe "set immediate 30 3"
	motor.exe "start immediate"
	motor.exe "stop"
	motor.exe "get mode"
For Linux:
	./motor motion.txt
	./motor "set profile 1 100 2"
	./motor "start profile 1"
	./motor "set immediate 30 3"
	./motor "start immediate"
	./motor "stop"
	./motor "get mode"	
	
	
### Who do I talk to? ###
Rahul Singh Bhauryal
rahulbhauryal@hotmail.com