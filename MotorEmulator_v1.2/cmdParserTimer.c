/**
  ******************************************************************************
  * @file      cmdParser.c
  * @brief      
  *            This module performs:
  *                - Init timer and see timeout for command
  ******************************************************************************
**/
#include <stdio.h>
#include <stdlib.h>
#include<string.h>
#include"main.h" 
#include "cmdParserTimer.h"
#if(OS == WINDOWS)
	#include <time.h> 
#else

#endif 

int TimerInd;
int TimerT[TIMER_MAX];

/**
  * @brief Init timer in sec here according to index
*/
void FillTimeoutInSec(int Ind, int T)
{	if(Ind < TIMER_MAX)
	{	TimerT[Ind] = T;	
	}
}
/**
  * @brief this function return status of timeout WAIT means: no timeout, TRUE=means there is timeout
*/
int TimeOut(int TimInd)	
{	int i = 0;
#if(OS == WINDOWS)	//windows time read here
	struct tm* current_time; time_t s;
	static int pSec; int sec;
	s = time(NULL); // time in seconds 
	current_time = localtime(&s); // to get current time
	sec = current_time->tm_sec;
#else

#endif	
	if((sec == (pSec+1)) || ((sec == 0) && (pSec == 59))) //PerSecCnter
	{	for(i = 0 ; i < TIMER_MAX; i++)
	  	{	if(TimerT[i] > 0)	//If there is value in timer then decrement by one sec
		  		TimerT[i] -= 1;  	  	
		}
	}
	pSec = sec;
	if(TimerT[TimInd] == 0) return TRUE;	//If time out happens then return true
	else return WAIT; 
}
