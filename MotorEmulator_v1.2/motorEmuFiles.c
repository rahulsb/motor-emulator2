/**
  ******************************************************************************
  * @file      motorEmuFiles.c
  * @brief      
  *            This module performs:
  *                - Init and update files for motor like profile and current settings 
  ******************************************************************************
**/
#include<stdio.h>
#include <stdlib.h>
#include <string.h>
#include"motorEmuFiles.h"
#include"motorEmu.h"
 
void InitFiles()
{	FILE *fp; 
	
	fp = fopen("saveCurrent.bin","r");
	if(fp != NULL) //if file exist then read current state of motor with the help of structre
	{	fread(&Motor,sizeof(MOTOR),1,fp);
	}
	else	//if file doesn't exists then create
	{	fclose(fp);
		fp = fopen("saveCurrent.bin","w"); 
	}
	fclose(fp);
}

void SaveCurrent()
{	FILE *fp; 
	fp = fopen("saveCurrent.bin","w");
	if(fp != NULL)
	{	fwrite(&Motor,sizeof(MOTOR),1,fp);
	}
	fclose(fp);
}

/**
  * @brief save a valid profile
*/
void SaveProfile(int ProfNo, int ProfVelocity, int ProfPosition)
{	if((ProfNo >= 0) && (ProfNo < MAX_PROFILE))
	{	Motor.ProfileV[ProfNo] = ProfVelocity;
		Motor.ProfileP[ProfNo] = ProfPosition;
	}
	
}

int ReadProfile(int ProfNo,unsigned int* ProfVelocity,unsigned int* ProfPosition)
{	if((ProfNo >= 0) && (ProfNo < MAX_PROFILE))
	{	if((Motor.ProfileV[ProfNo] >= 0)&&(Motor.ProfileV[ProfNo] >= 0))
		{	*ProfVelocity = Motor.ProfileV[ProfNo] ;
			*ProfPosition = Motor.ProfileP[ProfNo];
		}
		return (SUCCESS);
	}
	else
		return (FAIL);
}
