/**
  ******************************************************************************
  * @file      cmdParser.c
  * @brief      
  *            This module performs:
  *                - Process the command from command line or file
  *                - Detects valid command and value, And parse it
  *                - Send it to motor emulator 
  *                - Receive the response from motor emulator and process it
  *					- show warning or results according to response
  *					- maintain log
  ******************************************************************************
**/

#include <stdio.h>
#include <stdlib.h>
#include<string.h>
#include"main.h"
#include"cmdParser.h"
#include"cmdParserLogFile.h"
#include"cmdParserTimer.h"

enum{CMD_SET_P, CMD_SET_IM, CMD_START_PROFILE, CMD_START_IM, CMD_STOP, CMD_GET_PROFILE, CMD_GET_MODE, CMD_GET_ERR, CMD_GET_STATUS, CMD_RESET, CMD_MAX};
char *Cmd[] = {"set profile ", "set immediate ", "start profile ", "start immediate", "stop",
				"get profile ", "get mode",	"get error",	"get status",	"reset"};

/**
  * @brief Init command parser module
*/
void CmdParserInit()
{	InitLogFile();	//initilize log file logFile.txt
	TimeOut(TIMER_CMD_TIMEOUT);
}

/**
  * @brief Gets command line input or file name and resolve the valid command And assign it to assigner
  * @param str: command input in form of string
*/
void ProcessCmdParser(char *str)
{	int cmdNo , i;
	cmdNo = CommandResolver(str); //resolve command from command line string and returns valid command number
	if((cmdNo >= CMD_SET_P) && (cmdNo <= CMD_RESET)) //if valid command  number
	{	CommandAssigner(cmdNo, str);	//assigns command according to command number		
	}
	else	//if no valid command 
	{	i = SearchNProcessTxtFile(str); //search for .txt file 
		if(i == FALSE)	printf("Warning: Invalid command\n"); 
	}
}

/**
  * @brief Assign the commands according to communication objects
  			Extract the values for communication objects
  			process communication objects commans
  			saves log
  * @param cmdNo: Valid command no 
  * @param len: command line string
*/
void CommandAssigner(int cmdNo, char *str) //assign commands according to command no
{	int val1,val2,val3;//,Ack=0;
	CMD_ACK CmdAck;	//It consists of flag and data
	SaveLog(str);					//saves log according to command given
	printf("\n--------------Tx Module----------------\n");
	switch(cmdNo)
	{	case CMD_SET_P: val1 = ExtractValAfterSpace(str,2);	//extract value after 2nd space of command string. example: set profile 5 7 6 then val1 = 5
						val2 = ExtractValAfterSpace(str,3); //extract value after 3rd space of command string. example: set profile 5 7 6 then val2 = 7
						val3 = ExtractValAfterSpace(str,4); //extract value after 4ths space of command string
						if((val1 == INVALID_CMD) || (val2 == INVALID_CMD) || (val3 == INVALID_CMD))
						{	printf("Warning: Invalid command prameters\n");
							printf("set profile <profile number> <position in mm> <velocity in mm/s>\n");
						}	
						else
						{	printf("Setting profile number:%d\n",val1);
							CmdAck = ProcessObject(WRITE,I_C_OBJ_PROFIL,SI_PROFILE_NO,val1); //Write Profile number(val1) command to Rx module(motor)
							if(CmdAck.Flag == TRUE) //check Ack from previous cmd
							{	printf("Setting profile velocity:%d\n",val3);
								CmdAck = ProcessObject(WRITE,I_C_OBJ_PROFIL,SI_PROFILE_VELOCITY,val3); ////Write Profile velocity(val2) command to Rx module(motor)
								if(CmdAck.Flag== TRUE)
								{	printf("Setting profile position:%d\n",val2);
									ProcessObject(WRITE,I_C_OBJ_PROFIL,SI_PROFILE_POSITION,val2); //Write Profile Position(val3) command to Rx module(motor)
								}
							}
						}
						break;
		case CMD_SET_IM: 
						val1 = ExtractValAfterSpace(str,2);	 
						val2 = ExtractValAfterSpace(str,3);
						if((val1 == INVALID_CMD) || (val2 == INVALID_CMD))
						{	printf("Warning: Invalid command prameters\n");
							printf("set immediate <position in mm> <velocity in mm/s>\n");
						}	
						else
						{	printf("Setting immediate position:%d\n",val1);
							CmdAck = ProcessObject(WRITE,I_C_OBJ_IMMEDIATE,SI_IMMEDIATE_POSITION,val1); //Write Immediate position to Rx module(motor)
							printf("Setting immediate velocity:%d\n",val2);
							CmdAck = ProcessObject(WRITE,I_C_OBJ_IMMEDIATE,SI_IMMEDIATE_VELOCITY,val2); //Write Immediate velocity to Rx module(motor)														
						}
						break;
		case CMD_START_PROFILE: 
						val1 = ExtractValAfterSpace(str,2);	 
						if(val1 == INVALID_CMD)
						{	printf("Warning: Invalid command prameters\n");
							printf("start profile <profile number>\n");
						}	
						else
						{	printf("Setting Profile for run:%d\n",val1);
							CmdAck = ProcessObject(READ,I_C_OBJ_PROFIL,SI_PROFILE_NO,val1); //Read Profile status like All valid readings
							if(CmdAck.Flag == TRUE)
							{	printf("Start profile move\n");
								CmdAck = ProcessObject(WRITE,I_C_OBJ_OP_MODE,SI_OP_MODE,D_OP_MODE_SINGLE_P_MOVE); //Send Start single profile move to Rx module(motor)									
							}
						}
						break;										
		case CMD_START_IM:
						printf("Start immidiate motor\n");
						CmdAck = ProcessObject(WRITE,I_C_OBJ_OP_MODE,SI_OP_MODE,D_OP_MODE_IMMEDIATE_MOVE); //Write Immediate move to Rx module(motor)	
						
						break;
		case CMD_STOP:	printf("Stopping the motor\n");
						CmdAck = ProcessObject(WRITE,I_O_OBJ_CTL,SI_CTL,D_CTL_STOP); //Send Stop command to Rx module(motor)													
						break;
		case CMD_GET_PROFILE: 
						val1 = ExtractValAfterSpace(str,2);	 //Return Valid or Invalid command
						if(val1 == INVALID_CMD)
						{	printf("Warning: Invalid command prameters\n");
							printf("get profile <profile number>\n");
						}	
						else
						{	printf("Get profile number:%d\n",val1);
							CmdAck = ProcessObject(READ,I_C_OBJ_PROFIL,SI_PROFILE_NO,val1); //Inquire valid profile from Motor 
							if(CmdAck.Flag == TRUE)	//If valid profile
							{	CmdAck = ProcessObject(READ,I_C_OBJ_PROFIL,SI_PROFILE_VELOCITY,0); //Read Profile velocity from Rx module(motor), data can be anything ex. 0	
								val2 = CmdAck.Val;
								CmdAck = ProcessObject(READ,I_C_OBJ_PROFIL,SI_PROFILE_POSITION,0); //Read Profile position from Rx module(motor)	
								val3 = CmdAck.Val;
								printf(" Velocity = %d, Position = %d \n",val2,val3);	
							}							
						}
						break;
		case CMD_GET_MODE: 
						CmdAck = ProcessObject(READ,I_C_OBJ_OP_MODE,SI_OP_MODE,0);	//Read current working mode of motor						
						switch(CmdAck.Val)
						{	case D_OP_MODE_IMMEDIATE_MOVE:	printf(">Current mode is Immediate move.\n");break;
							case D_OP_MODE_SINGLE_P_MOVE:	printf(">Current mode is Single profile move.\n");break;
							case D_OP_MODE_CYCLIC_P_MOVE:	printf(">Current mode is Cyclic profile move.\n");break;
							default: printf(">Current mode unknown.\n");break;
						} 
						break;
		case CMD_GET_ERR:
						CmdAck = ProcessObject(READ,I_C_OBJ_ERR_CODE,SI_ERROR_SUB_IND,0); //Read Errors of motor
						if(CmdAck.Val == 0) printf(">No errors.\n");
						else ShowErrWarning(val1);
						break;								
		case CMD_GET_STATUS:
						CmdAck = ProcessObject(READ,I_O_OBJ_STATUS,SI_STATUS,0); //Read current status of motor
						switch(CmdAck.Val)
						{	case D_STATUS_MOV_ACT:	printf(">Status: Move active.\n");break;
							case D_STATUS_ERR:	printf(">Status: Error.\n");break;
							default: printf(">Status unknown.\n");break;
						}  
						break;
		case	CMD_RESET:
						//CmdAck = ProcessObject(WRITE,I_C_OBJ_ERR_CODE,SI_ERROR_SUB_IND,0); //claers error code
						CmdAck = ProcessObject(WRITE,I_O_OBJ_CTL,SI_CTL,D_CTL_ERR_CLR); //claers error code
						if(CmdAck.Flag == TRUE) printf(">Reset: Error code cleared\n");
						CmdAck = ProcessObject(WRITE,I_C_OBJ_OP_MODE,SI_OP_MODE,D_OP_MODE_IMMEDIATE_MOVE); //claers error code
						if(CmdAck.Flag == TRUE) printf(">Reset: Set to immediate mode.\n");
						break;
		default: 
							printf("Warning: Invalid command\n"); 
						break;				
	}
	printf("---------------------------------------\n");
}
 
/**
  * @brief Process the state machine for a command
  			1.Send request
  			2.get Answer
  			3.Read status word
  			4.check error flag
  			5.read error code
  			6.clear error 
  * @param Mode: READ / WRITE
  * @param ObjectId: object index
  * @param SubID: object subindex
  * @param Data: for WRITE : a valid data
  				for READ: can be any value it doesn't matter
  * @retval CMD_ACK: frlag and data(READ)
*/
enum{ STATE_SEND_REQ, STATE_GET_ANS, STATE_READ_STATUS, STATE_CHECK_ERR_FLG, STATE_READ_ERR_CODE,STATE_CLEAR_ERR, STATE_CLEAR_ERR_RESP}; 
CMD_ACK ProcessObject(char Mode, unsigned int ObjectId, unsigned char SubID, unsigned int Data)
{	int state = STATE_SEND_REQ, exit = 0;
	unsigned int RxData=0,  data = 0; 
	CMD_ACK CmdAck;
	CmdAck.Val = 0; CmdAck.Flag = ERROR1;
	
	while(1)
	{	switch(state)
		{	case STATE_SEND_REQ:	SendCommand(Mode,ObjectId,SubID,Data,10); state = STATE_GET_ANS;
									break;		
			case STATE_GET_ANS:		CmdAck.Flag = CPReadResponse(&RxData);//wait for response
									if(CmdAck.Flag == TRUE) 
									{	data = RxData;
										CmdAck.Val = RxData;
										state = STATE_READ_STATUS;
									}
									else if(CmdAck.Flag == FALSE)  printf("*Error: CRC not matched\n");
									else if(CmdAck.Flag == TIMEOUT) { exit = 1; printf("*Timeout: motor emulator not responding.\n");}
									break;					
			case STATE_READ_STATUS:	SendCommand(READ,I_O_OBJ_STATUS,SI_STATUS,0,10); state = STATE_CHECK_ERR_FLG;
									break;
			case STATE_CHECK_ERR_FLG:
									CmdAck.Flag = CPReadResponse(&RxData);//wait for response
									if(CmdAck.Flag == TRUE) 
									{	if(RxData & 1){ printf("***  Error Status  ***\n");  }
										else 
										{	exit = 1; return(CmdAck);//return(data);
										}
										state = STATE_READ_ERR_CODE;
									}
									else if(CmdAck.Flag == FALSE) printf("*Error: CRC not matched\n");
									else if(CmdAck.Flag == TIMEOUT) { exit = 1; printf("*Timeout: motor emulator not responding.\n");}
																		
									break;									
			case STATE_READ_ERR_CODE:
									SendCommand(READ,I_C_OBJ_ERR_CODE,SI_ERROR_SUB_IND,0,10); 
									state = STATE_CLEAR_ERR;									
									break;
			case STATE_CLEAR_ERR:	CmdAck.Flag = CPReadResponse(&RxData);//wait for response
									if(CmdAck.Flag == TRUE) 
									{	ShowErrWarning(RxData);
										SendCommand(WRITE,I_O_OBJ_CTL,SI_CTL,D_CTL_ERR_CLR, 10); 
										state = STATE_CLEAR_ERR_RESP;
									}
									else if(CmdAck.Flag == FALSE) printf("*Error: CRC not matched\n");
									else if(CmdAck.Flag == TIMEOUT) { exit = 1; printf("*Timeout: motor emulator not responding.\n");}
									break;
			case STATE_CLEAR_ERR_RESP:
									CmdAck.Flag = CPReadResponse(&RxData);//wait for response
									if(CmdAck.Flag == TRUE) 
									{	exit = 1;
										printf("***  Error cleared  ***\n");
									}
									else if(CmdAck.Flag == FALSE) printf("*Error: CRC not matched\n");
									else if(CmdAck.Flag == TIMEOUT) { exit = 1; printf("*Timeout: motor emulator not responding.\n");}
									CmdAck.Flag = ERROR1;
									return(CmdAck);//return(-1); 
			default: break;																		
		}
		if(exit)
		{	break;	
		}
	}
	CmdAck.Flag = ERROR1;
	return(CmdAck);
	//return(-1);
}
/**
  * @brief Calculate CRC and send data
  * @param Mode: READ / WRITE
  * @param ObjectId: object index
  * @param SubID: object subindex
  * @param Data: for WRITE : a valid data
  				for READ: can be any value it doesn't matter
  * @param TimeoutSec: Timeout for a command
*/
#define CMD_LEN 8
unsigned char ToMotor[CMD_LEN]; 
void SendCommand(char Mode, unsigned int ObjectId, unsigned char SubID, unsigned int Data,int TimeoutSec)
{	unsigned int CkSum = 0, i = 0, j = 0;
	ToMotor[i++] = (ObjectId >> 8) & 0xFF;
	ToMotor[i++] = ObjectId & 0xFF;
	ToMotor[i++] = SubID;
	Data |= (Mode << 31);
	ToMotor[i++] = (Data >> 24) & 0xFF;
	ToMotor[i++] = (Data >> 16) & 0xFF;
	ToMotor[i++] = (Data >> 8) & 0xFF;
	ToMotor[i++] = Data & 0xFF;
	for(j=0;j<i;j++) CkSum += ToMotor[j];
	ToMotor[i] = CkSum & 0xFF;
	FillTimeoutInSec(TIMER_CMD_TIMEOUT,TimeoutSec); 
	openAPICmdParserToMotorEmu(ToMotor,8);	//send to API here
}

/**
  * @brief Extract value from after space spacepos from string
  * @param str: string with spaces
  * @param SpacePos: value after this value in string
  * @retval int value after space
*/
int ExtractValAfterSpace(char *Str, int SpacePos)	//Parse command line data values 
{	unsigned int num=0, pos = 0, i = 0, maxLen = strlen(Str);
	char *lcPtr = Str; char numChar[16];
	pos = 0;	
	
	for(i = 0; i < SpacePos; i++) //get the pointer to SpacePos(on function call) space
	{	pos = pos + (char*)memchr(lcPtr,' ',strlen(lcPtr)) - lcPtr + 1;
		if(pos > maxLen) { return(INVALID_CMD); }
		for(; pos < maxLen; pos++) if(Str[pos] != ' ') {lcPtr = &Str[pos]; break;}  //to escape extra space 
	} 
	
	i = 0;
	while((lcPtr[i] != 0) && (lcPtr[i] != ' ')) //get the string after space till next space or NULL
	{	numChar[i] = lcPtr[i];  i++;
	}
	numChar[i] = 0;	//Null to num string
	
	num = atoi(numChar); //convert integer to num
	return(num);
}

/**
  * @brief Search commands from command line file, seprated by new line
  * @param str: .txt file name
  * @retval int value for valid invalid file
*/
int SearchNProcessTxtFile(char *str)	//search and send command accordingly .Txt file
{	int i=0, cmdNo, l1 = strlen(str), l2 = strlen(".txt");
	char cmd[128], c;
	int status = FALSE;
	FILE *fp;
	if(strcmp(&str[l1-4],".txt") == 0) //if .txt in it then parse command
	{	fp = fopen(str,"r"); 				
		if(fp != NULL)					//valid file existed ?
		{	printf("File found\n"); 
			c = getc(fp);//read single character
			
			while(c != EOF)	//save data to array
			{	cmd[i++] = (char)c;
				if(c == '\n') 	//if one line scan complete
				{ 	cmd[i] = 0; //add null at end
					cmdNo = CommandResolver(cmd); //resolve valid command number
					if((cmdNo >= CMD_SET_P) && (cmdNo <= CMD_RESET)) //for valid command
					{	CommandAssigner(cmdNo, cmd);
						i=0;  
					}
					else
					{	if(cmd[0] != '\n') //if only new line
							printf("Invalid Command.: %s\n",cmd);
						i = 0;
					}
					
				}			
				c = (char)getc(fp);	//gets single character
			}  
			status = TRUE;
		}	
		else	//if file not found
		{	printf("File not found\n");
			status = FALSE;
		}	
		fclose(fp);
	}
	return status;
} 
/**
  * @brief resolve command according to string input from command line or text file
  * @param str: string with spaces
  * @retval int value for command number index
*/
int CommandResolver(char *str)	//
{	int i, n = 0, cmdNo = -1, tmp = 0;
	for(i = 0 ; i < CMD_MAX ; i++) //get command index using string compare
	{	n = strlen(Cmd[i]);
		if(strncmp(str,Cmd[i],n) == 0)	//search for valid command
		{	if((i==CMD_START_IM) || (i==CMD_STOP) ||  (i==CMD_GET_MODE) || (i==CMD_GET_ERR) || (i==CMD_GET_STATUS) || (i==CMD_RESET) ) //confirm string for no aad-on char
			{	if((str[n] == 0) || (str[n] == '\n')) { cmdNo = i; break; } 
			}
			else //command having payload
			{	cmdNo = i; break;
			}
		} 
	}
	return  cmdNo;
}

/**
  * @brief Print warnings
*/

void ShowErrWarning(int Err)	//print warnings
{	int i = 0, tmp = 0;
	switch(Err)
	{	case  D_ERR_OBJ_NOT_EXISTS: printf("***Error: object does not exist***\n"); break;
		case  D_ERR_OBJ_READ_ONLY:	printf("***Error: object is read-only***\n"); break;
		case  D_ERR_OBJ_WRITE_ONLY:	printf("***Error: object write only***\n"); break;
		case  D_ERR_VALUE_TOO_HIGH:	printf("***Error: value too high***\n"); break;
		case  D_ERR_VALUE_TOO_LOW:	printf("***Error: value too low***\n"); break;
		case  D_ERR_IMMEDIATE_V_0:	printf("***Error: immediate velocity is 0***\n"); break;
		case  D_ERR_NO_PROF_CONFIG:	printf("***Error: no profiles configured***\n"); break;
		default:break;
	}
	
}

//----------------------------Response-------------------------------------
#define RX_BUF_SZ 32
unsigned char Rx2Buf[RX_BUF_SZ];
int Rx2Due = 0, Rx2Rd = 0, Rx2Wr = 0;
/**
  * @brief takes value from RXmodule ot motor and fill buffer
  * @param val: Byte value to fill buffer
*/

void FillCmdParserRxBuf(unsigned char Val) 
{	Rx2Buf[Rx2Wr] = Val;
	Rx2Due = Rx2Due + 1; Rx2Wr = (Rx2Wr+1)&(RX_BUF_SZ-1);
}
/**
  * @brief read response from motor parser module(motor) data
  * @param *Data: to place returned value from motor
  * @retval int value for status
*/

int CPReadResponse(unsigned int *Data)
{	static int state = 0;
	unsigned char data = 0;
	static unsigned int rxObjID = 0, rxSubID = 0, rxData = 0, ckSum = 0; 
	while(Rx2Due)	//check Rx2Due. if there is some unread data in Rx2Buf[] 
	{	data = Rx2Buf[Rx2Rd];	//read data
		Rx2Due = Rx2Due - 1; Rx2Rd = (Rx2Rd+1)&(RX_BUF_SZ-1); //decrement Rx2Due, Increase Read pointer 
		
		switch(state) 	//State machine for received data from motor 
		{	case OBJ_ID_MSB_IND:	if((data == 0x10) || (data == 0x60))	//to check object index MSB as Sync byte
									{	rxObjID = data;	ckSum = data;	state++;	
									}
									break;
			case OBJ_ID_LSB_IND:	rxObjID = (rxObjID << 8) + data;ckSum += data;	state++;	break;
			case SUB_ID_IND:		rxSubID = data;					ckSum += data;	state++;	break;
			case DATA_D3_IND:		rxData = data;					ckSum += data;	state++;	break;
			case DATA_D2_IND:		rxData = (rxData << 8) + data; 	ckSum += data;	state++;	break;
			case DATA_D1_IND:		rxData = (rxData << 8) + data;	ckSum += data;	state++;	break;
			case DATA_D0_IND:		rxData = (rxData << 8) + data;	ckSum += data;	state++;	break;
			case CKSUM_IND:		state = OBJ_ID_MSB_IND;
								if(data == (ckSum & 0xFF))
								{	*Data = rxData;
									return(TRUE);									
								} 
								return(FALSE);
								//break;
			default:	state = OBJ_ID_MSB_IND;	break;					
		}
	}
	if(TimeOut(TIMER_CMD_TIMEOUT) == TRUE) 
	{	return(TIMEOUT);
	}	
	return(WAIT);
}



