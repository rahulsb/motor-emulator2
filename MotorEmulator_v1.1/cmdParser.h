#ifndef _TX
#define _TX

typedef struct
{	int Flag; int Val;
}CMD_ACK; 

void TxModuleInit(void);
int CommandResolver(char *);
int TMReadResponse(unsigned int*);
void FillTxModuleRxBuf(unsigned char );
void SendCommand(char , unsigned int , unsigned char , unsigned int, int );
CMD_ACK ProcessCommand(char , unsigned int , unsigned char , unsigned int );
void ShowErrWarning(int );
void SaveLog(char *);
void CommandAssigner(int Cmd, char *);
void ProcessTxModule(char *);
int SearchNProcessTxtFile(char *);

#endif

