#include <stdio.h>
#include <stdlib.h>
#include"main.h"
#include"motorEmu.h"
#include"motorEmuFiles.h"


#define RX_BUF_SZ 32
MOTOR Motor;
unsigned char RxBuf[RX_BUF_SZ];
int RxDue = 0, RxRd = 0, RxWr = 0;

void RxModuleInit()
{	InitFiles();	//initilize files like profile.txt and saveCurrent.bin for saving current state
}

void FillRxModuleRxBuf(unsigned char Val) //fill values from command parser module
{	RxBuf[RxWr] = Val;		//place the value in buffer
	RxDue = RxDue + 1; RxWr = (RxWr+1)&(RX_BUF_SZ-1); //Increment Due and write pointer
}

void ProcessRxModule()
{	RMReadCommand();	//Read command if avilable by looking at Due
}

int RMReadCommand()
{	static int state = 0;
	unsigned char data = 0;
	static unsigned int rxObjID = 0, rxSubID = 0, rxData = 0, ckSum = 0;
	while(RxDue)	//If due Read data
	{	data = RxBuf[RxRd]; 
		RxDue = RxDue - 1; RxRd = (RxRd+1)&(RX_BUF_SZ-1); //Decrement Due and Increment read pointer
		
		switch(state) 
		{	case OBJ_ID_MSB_IND:	if((data == 0x10) || (data == 0x60))	//to check object index MSB as Sync byte
									{	rxObjID = data;	ckSum = data;	state++;
									}
									break;
			case OBJ_ID_LSB_IND:	rxObjID = (rxObjID << 8) + data;ckSum += data;	state++;	break;
			case SUB_ID_IND:		rxSubID = data;					ckSum += data;	state++;	break;
			case DATA_D3_IND:		rxData = data;					ckSum += data;	state++;	break;
			case DATA_D2_IND:		rxData = (rxData << 8) + data; 	ckSum += data;	state++;	break;
			case DATA_D1_IND:		rxData = (rxData << 8) + data;	ckSum += data;	state++;	break;
			case DATA_D0_IND:		rxData = (rxData << 8) + data;	ckSum += data;	state++;	break;
			case CKSUM_IND:		if(data == (ckSum & 0xFF))
								{	ProcessRxData(rxObjID, rxSubID, rxData);									
								}
								else
								{	//send cksum err
								}
								state = OBJ_ID_MSB_IND;
								return(TRUE);
								break;
			default:	state = OBJ_ID_MSB_IND;	 break;					
		}
	}
	return(FALSE);
}

void SetErrFlag(int Err)
{	Motor.Status |= D_STATUS_ERR; //Set Error Status in status register
	Motor.Error = Err;	//Set error number
}
void ProcessRxData(unsigned int ObjectId, unsigned char SubID, unsigned int Data)
{	int ack = -1;
	int Tmp = 0;
	static int TmpProfileNo = -1; 
	switch(ObjectId)	//Go to the respective object ID case 
	{	case I_C_OBJ_OP_MODE:	if(SubID == SI_OP_MODE) //chekck sub ID here
								{	if(Data & (1 << 31)) //Check if write mode
									{	Tmp = Data & ~(1 << 31); //Clear Write flag bit from data
										switch(Tmp)
										{	case D_OP_MODE_IMMEDIATE_MOVE:	if(Motor.ImdVelocity != 0)
																			{	Motor.CurrVelocity = Motor.ImdVelocity; 
																				Motor.CurrPos = Motor.ImdPos; 
																				Motor.Control = START; 
																				Motor.OprationMode = Tmp;
																			}
																			break;									
											case D_OP_MODE_SINGLE_P_MOVE:	if(Motor.PrfVelocity != 0) //then profile not active
																			{	Motor.CurrVelocity = Motor.PrfVelocity; 
																				Motor.CurrPos = Motor.PrfPos; 
																				Motor.Control = START; 
																				Motor.OprationMode = Tmp;
																			}
																			break;
											case D_OP_MODE_CYCLIC_P_MOVE:	break;	
										}
									}
								}								
								else
								{	SetErrFlag(D_ERR_OBJ_NOT_EXISTS);
								}
								SendResponse(ObjectId, SubID, Motor.OprationMode);
								break;	
		case I_C_OBJ_IMMEDIATE:	if(SubID == SI_IMMEDIATE_VELOCITY)
								{	if(Data & (1 << 31)) //Check if write mode	
									{	Tmp = Data & ~(1 << 31); //Clear Write flag bit from data
										if(Tmp >= (1 <<  30)) Tmp -= (1 << 31);  //convert signed number										
										if((Tmp > MIN_VELOCITY) && (Tmp <= MAX_VELOCITY)) {Motor.ImdVelocity = Tmp;}
										else if(Tmp < MIN_VELOCITY) { SetErrFlag(D_ERR_VALUE_TOO_LOW); }
										else if(Tmp > MAX_VELOCITY) { SetErrFlag(D_ERR_VALUE_TOO_HIGH); }
										
										if(Tmp == 0)
										{	SetErrFlag(D_ERR_IMMEDIATE_V_0);
										}
									}
									SendResponse(ObjectId, SubID, Motor.ImdVelocity);
								}
								else if(SubID == SI_IMMEDIATE_POSITION)
								{	if(Data & (1 << 31)) //Check if write mode
									{	Tmp = Data & ~(1 << 31); //Clear Write flag bit from data
										if(Tmp >= (1 <<  30)) Tmp -= (1 << 31);  //convert signed number
										if((Tmp >= MIN_POSITION) && (Tmp <= MAX_POSITION)) {Motor.ImdPos = Tmp;}
										else if(Tmp < MIN_POSITION) { SetErrFlag(D_ERR_VALUE_TOO_LOW); }
										else if(Tmp > MAX_POSITION) { SetErrFlag(D_ERR_VALUE_TOO_HIGH); }										
									}
									SendResponse(ObjectId, SubID, Motor.ImdPos);
								} 								
								else
								{	SetErrFlag(D_ERR_OBJ_NOT_EXISTS);
									SendResponse(ObjectId, SubID, Data);
								}
								break;
		case I_C_OBJ_PROFIL:	if(SubID == SI_PROFILE_NO)
								{	Tmp = Data & ~(1 << 31); //Clear Write flag bit from data
									if(Data & (1 << 31)) //Check if write mode
									{	if(Tmp >= MAX_PROFILE) { SetErrFlag(D_ERR_NO_PROF_CONFIG); }										
										else
										{	Motor.PrfNo	= Tmp;
										}
										TmpProfileNo = Tmp;
									}
									else
									{	if(Tmp >= MAX_PROFILE) { SetErrFlag(D_ERR_NO_PROF_CONFIG); }										
										else
										{	Motor.PrfNo	= Tmp;
											ack = ReadProfile(Motor.PrfNo,&Motor.PrfVelocity,&Motor.PrfPos); 
											if((ack == PROFILE_NA) || (ack == FAIL))
											{	SetErrFlag(D_ERR_NO_PROF_CONFIG);
											}
											else
											{	
											}	
										}
									}
									SendResponse(ObjectId, SubID, Motor.PrfNo);
								}
								else if(SubID == SI_PROFILE_VELOCITY)
								{	if(Data & (1 << 31)) //Check if write mode
									{	Tmp = Data & ~(1 << 31); //Clear Write flag bit from data
										if(Tmp >= (1 <<  30)) Tmp -= (1 << 31);  //convert signed number
										if((Tmp > MIN_VELOCITY) && (Tmp <= MAX_VELOCITY)) {Motor.PrfVelocity = Tmp;}
										else if(Tmp < MIN_VELOCITY) { SetErrFlag(D_ERR_VALUE_TOO_LOW); }
										else if(Tmp > MAX_VELOCITY) { SetErrFlag(D_ERR_VALUE_TOO_HIGH); }
									}
									SendResponse(ObjectId, SubID, Motor.PrfVelocity);
								}
								else if(SubID == SI_PROFILE_POSITION)
								{	if(Data & (1 << 31)) //Check if write mode
									{	Tmp = Data & ~(1 << 31); //Clear Write flag bit from data
										if(Tmp >= (1 <<  30)) Tmp -= (1 << 31);  //convert signed number
										if((Tmp >= MIN_POSITION) && (Tmp <= MAX_POSITION)) {Motor.PrfPos = Tmp;}
										else if(Tmp < MIN_POSITION) { SetErrFlag(D_ERR_VALUE_TOO_LOW); }
										else if(Tmp > MAX_POSITION) { SetErrFlag(D_ERR_VALUE_TOO_HIGH); }
										
										if(TmpProfileNo < MAX_PROFILE) 
											SaveProfile(Motor.PrfNo,Motor.PrfVelocity,Motor.PrfPos);
										 
									}
									SendResponse(ObjectId, SubID, Motor.PrfPos);
								}
								else
								{	SetErrFlag(D_ERR_OBJ_NOT_EXISTS);
									SendResponse(ObjectId, SubID, Data);
								} 			
								break;
		case I_C_OBJ_ERR_CODE:	if(SubID == 0x00)
								{	if(Data & (1 << 31)) //Check if write mode
									{	SetErrFlag(D_ERR_OBJ_READ_ONLY);
									}
								}
								else
								{	SetErrFlag(D_ERR_OBJ_NOT_EXISTS);
								}
								SendResponse(ObjectId, SubID, Motor.Error); 
								break;
		case I_O_OBJ_CTL:		if(SubID == SI_CTL)
								{	if(Data & (1 << 31)) //Check if write mode
									{	switch(Data & 3)
										{	case D_CTL_START: 	Motor.Control = START;  break;
											case D_CTL_STOP: 	Motor.Control = STOP;  break;
											case D_CTL_ERR_CLR: Motor.Error = 0; break;
											default:break;
										}
									}
									else
									{	SetErrFlag(D_ERR_OBJ_WRITE_ONLY);
									}									
								}
								else
								{	SetErrFlag(D_ERR_OBJ_NOT_EXISTS);
								}
								SendResponse(ObjectId, SubID, Motor.Control);
								break;
		case I_O_OBJ_STATUS:	if(SubID == 0x00)
								{	if(Data & (1 << 31)) //Check if write mode
									{	SetErrFlag(D_ERR_OBJ_READ_ONLY);
									}
									else
									{	if(Motor.Error == 0) Motor.Status = 0;										
									}
								}
								else
								{	SetErrFlag(D_ERR_OBJ_NOT_EXISTS);
								}
								SendResponse(ObjectId, SubID, Motor.Status);
								break;
		case I_O_OBJ_CURRENT:	if(SubID == SI_CURRENT_POS)
								{	if(Data & (1 << 31))  SetErrFlag(D_ERR_OBJ_READ_ONLY);									
									SendResponse(ObjectId, SubID, Motor.CurrVelocity);
								}
								if(SubID == SI_CURRENT_VEL)
								{	if(Data & (1 << 31))  SetErrFlag(D_ERR_OBJ_READ_ONLY);									
									SendResponse(ObjectId, SubID, Motor.CurrPos);
								}
								else
								{	SetErrFlag(D_ERR_OBJ_NOT_EXISTS);
									SendResponse(ObjectId, SubID, Data);
								}
								break;
		default:				SetErrFlag(D_ERR_OBJ_NOT_EXISTS);
								SendResponse(ObjectId, SubID, Data);
								break;																														
	}
	SaveCurrent();	
}

void SendResponse(unsigned int ObjectId, unsigned char SubID, unsigned int Data)
{	unsigned char respBuf[8];
	unsigned int ckSum = 0, i = 0, j = 0;
	respBuf[i++] = (ObjectId >> 8) & 0xFF;
	respBuf[i++] = ObjectId & 0xFF;
	respBuf[i++] = SubID;
	respBuf[i++] = (Data >> 24) & 0xFF;
	respBuf[i++] = (Data >> 16) & 0xFF;
	respBuf[i++] = (Data >> 8) & 0xFF;
	respBuf[i++] = Data & 0xFF;
	for(j=0;j<i;j++) ckSum += respBuf[j];
	respBuf[i] = ckSum & 0xFF;
	
	//send API here  
	openAPIRxToTx(respBuf,8);
}


void PrintMotorCurrentState()	//print motor current status
{	printf("------------------Rx Module----------------\n");
	switch(Motor.OprationMode)
	{	case D_OP_MODE_IMMEDIATE_MOVE:	printf("Rx:Opration Mode: Immediate move.\n"); break;
		case D_OP_MODE_SINGLE_P_MOVE:	printf("Rx:Opration Mode: Single Profile move.\n"); break;
		case D_OP_MODE_CYCLIC_P_MOVE:	printf("Rx:Opration Mode: Cyclic Profile move.\n"); break;
	}
	printf("Rx:Current velocity: %d, Current Position: %d\n",Motor.CurrVelocity,Motor.CurrPos);
	if(Motor.Control == START) printf("Rx:Motor Status: RUNNING\n");
	else printf("Rx:Motor Status: STOPPED\n");
	printf("-------------------------------------------\n");
}
