#ifndef _RX
#define _RX

#define MAX_PROFILE 10
typedef struct{
	unsigned int OprationMode;
	unsigned int ImdVelocity; //Immediate velocity
	unsigned int ImdPos; //Immediate Position
	unsigned int PrfNo;	//profile No
	unsigned int PrfVelocity; //profile velocity
	unsigned int PrfPos; //profile Position
	unsigned int CurrVelocity; //profile velocity
	unsigned int CurrPos; //profile Position
	
	unsigned int Error;
	unsigned int Status; 
	unsigned int Control;
	
	unsigned int ProfileV[MAX_PROFILE];
	unsigned int ProfileP[MAX_PROFILE];
}MOTOR;
extern MOTOR Motor;

#define START			1
#define STOP			0

#define MIN_VELOCITY	0 //mm/sec
#define MAX_VELOCITY	10 //mm/sec
#define MIN_POSITION	0 //mm
#define MAX_POSITION	1000 //mm
#define MAX_PROFILE		10 //from 0 to 9

 
void RxModuleInit(void);
void FillRxModuleRxBuf(unsigned char);
void ProcessRxModule(void);
int RMReadCommand(void);
void ProcessRxData(unsigned int , unsigned char , unsigned int );
void PrintMotorCurrentState();
void SendResponse(unsigned int , unsigned char , unsigned int );	
#endif

