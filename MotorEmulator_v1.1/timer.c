#include <stdio.h>
#include <stdlib.h>
#include<string.h>
#include"main.h" 
#include "timer.h"
#if(OS == WINDOWS)
	#include <time.h> 
#else

#endif 

int TimerInd;
int TimerT[TIMER_MAX];

void FillTimeoutInSec(int Ind, int T)	//Init timer here according to index
{	if(Ind < TIMER_MAX)
	{	TimerT[Ind] = T;
	}
}
int TimeOut(int TimInd)
{	int i = 0;
#if(OS == WINDOWS)	//windows time read here
	struct tm* current_time; time_t s;
	static int pSec; int sec;
	s = time(NULL); // time in seconds 
	current_time = localtime(&s); // to get current time
	sec = current_time->tm_sec;
#else

#endif	
	if((sec == (pSec+1)) || ((sec == 0) && (pSec == 59))) //PerSecCnter
	{	for(i = 0 ; i < TIMER_MAX; i++)
	  	{	if(TimerT[i] > 0)
		  		TimerT[i] -= 1;  	  	
		}
	}
	pSec = sec;
	if(TimerT[TimInd] == 0) return TRUE;
	else return WAIT; 
}
