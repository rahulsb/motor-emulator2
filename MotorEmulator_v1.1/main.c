#include <stdio.h>
#include <stdlib.h>
#include "main.h"
#include "cmdParser.h"
#include "motorEmu.h"

int openAPITxToRx(char *ch, int len) //TX to API function will come here
{	int i=0;
	for( i = 0 ; i < len ; i++)
	{	FillRxModuleRxBuf(ch[i]);
	} 
	ProcessRxModule();
}
int openAPIRxToTx(char *ch, int len)//RX from API function will come here
{	int i=0;
	for( i = 0 ; i < len ; i++)
	{	FillTxModuleRxBuf(ch[i]);
	} 
} 

int main(int argc, char* argv[]) 
{  	TxModuleInit();	//Initialize Tx module dependencies
	RxModuleInit();	//Initialize Rx(motor side) module dependencies
	if( argc == 2 ) //Rx command from command line	
	{	ProcessTxModule(argv[1]);	//process the module command and send it to motor side
   	}
   	else
   	{		
   		printf("Warning : Please pass a valid command\n");
	}
	PrintMotorCurrentState();	//print motor current state
	return 0;
}
