#ifndef _TX
#define _TX

typedef struct
{	int Flag; int Val;
}CMD_ACK; 

void TxModuleInit(void);
int CommandResolver(char *);
int TMReadResponse(unsigned int*);
void FillTxModuleRxBuf(unsigned char val);
void SendCommand(char Mode, unsigned int ObjectId, unsigned char SubID, unsigned int Data);
CMD_ACK ProcessCommand(char Mode, unsigned int ObjectId, unsigned char SubID, unsigned int Data);
void ShowErrWarning(int );
void SaveLog(char *Str);
void CommandAssigner(int Cmd, char *);
void ProcessTxModule(char *str);
int SearchTxtFile(char *);

#endif

