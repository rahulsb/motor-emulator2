#ifndef _RX
#define _RX

typedef struct{
	unsigned int OprationMode;
	unsigned int ImdVelocity; //Immediate velocity
	unsigned int ImdPos; //Immediate Position
	unsigned int PrfNo;	//profile No
	unsigned int PrfVelocity; //profile velocity
	unsigned int PrfPos; //profile Position
	unsigned int CurrVelocity; //profile velocity
	unsigned int CurrPos; //profile Position
	
	unsigned int Error;
	unsigned int Status;
	//unsigned int Start;
	unsigned int Control;
}RXMODULE;
extern RXMODULE RxMod;

#define START			1
#define STOP			0

#define MIN_VELOCITY	0 //mm/sec
#define MAX_VELOCITY	10 //mm/sec
#define MIN_POSITION	0 //mm
#define MAX_POSITION	1000 //mm
#define MAX_PROFILE		10


//extern unsigned char RxBuf[RX_BUF_SZ];
//extern int RxDue, RxRd, RxWr;

void RxModuleInit(void);
void FillRxModuleRxBuf(unsigned char);
void ProcessRxModule(void);
int RMReadCommand(void);
void ProcessRxData(unsigned int ObjectId, unsigned char SubID, unsigned int Data);
void PrintMotorCurrentState();	
#endif

