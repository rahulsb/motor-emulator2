#include<stdio.h>
#include <stdlib.h>
#include <string.h>
#include"RxModuleFiles.h"
#include "RxModule.h"

#define MAX 512
void InitFiles()
{	FILE *fp;
	fp = fopen("profile.txt","r");
	if(fp != NULL) //if file doesn't exists then create
	{	
	}
	else
	{	fclose(fp);
		fp = fopen("profile.txt","w");
		//fprintf(fp, "ProfileNo   Velocity(mm/s)    Position(mm)\n");
		//fprintf(fp,">\n>\n>\n>\n>\n>\n>\n>\n>\n>\n>\n");
		 
	}
	fclose(fp); 
	
	fp = fopen("saveCurrent.bin","r");
	if(fp != NULL) //if file doesn't exists then create
	{	fread(&RxMod,sizeof(RXMODULE),1,fp);
	}
	else
	{	fclose(fp);
		fp = fopen("saveCurrent.bin","w"); 
	}
	fclose(fp);
}

void SaveCurrent()
{	FILE *fp; 
	fp = fopen("saveCurrent.bin","w");
	if(fp != NULL)
	{	fwrite(&RxMod,sizeof(RXMODULE),1,fp);
	}
	fclose(fp);
}

void SaveProfile(int ProfNo, int ProfVelocity, int ProfPosition)
{	FILE *fp;
	char str[11][64], c, tmpStr[64], i = 0, j = 0;
	fp = fopen("profile.txt","r");
	if(fp != NULL)
	{ 	c = getc(fp);
		while(c != EOF)	//save previous data to array
		{	str[i][j++] = (char)c;
			if(c == '\n') { str[i][j] = 0; j=0; i++; }			
			c = (char)getc(fp);
		} 
		//copy data to perticular file
	//	sprintf(str[ProfNo+1],">   %d            %d              %d\n",ProfNo,ProfVelocity,ProfPosition);
	//	for(i=0;i<(MAX_PROFILE+1);i++) printf("%s",str[i]); 
		fclose(fp);
		
		//rewrite files
		fp = fopen("profile.txt","w");
		for(i=0;i<(MAX_PROFILE+1);i++)
		{	for(j=0;j<64;j++)	
			{	putc(str[i][j],fp);
				if(str[i][j] == '\n') break;
			}
		}
		fclose(fp);
	}
	else //if first time file is not created
	{	printf("Error: profile.txt missing");  
	} 
	fclose(fp);
}

int ReadProfile(int ProfNo,unsigned int* ProfVelocity,unsigned int* ProfPosition)
{	FILE *fp;
	char str[11][64], c, tmpStr[64], i = 0, j = 0, k = 0;
	int flag = -1, Out[3] = {-1,-1,-1}, m = 0;
	
	fp = fopen("profile.txt","r");
	if(fp != NULL)
	{ 	c = getc(fp);
		while(c != EOF) //read all lines and store in 2D array according to new line
		{	str[i][j++] = (char)c;
			if(c == '\n') { str[i][j] = 0; j=0; i++; }			
			c = (char)getc(fp);
		}
				
		i = ProfNo + 1;
		for(j=0;j<64;j++) //search the respective line	
		{	if((str[i][j] >= '0') && (str[i][j] <= '9'))
			{	tmpStr[k++] = str[i][j];
			}
			else 
			{	if(k > 0)
				{	tmpStr[k] = 0; k = 0;
					Out[m++] = atoi(tmpStr);
				}
			} 
		}
		if(j == 64) flag = PROFILE_NA;
		fclose(fp);
	}
	else //if first time file is not created
	{	printf("Profile File dow=esn't exists");  
	}
	fclose(fp);
	if((Out[1] > 0) && (Out[2] > 0))
	{	*ProfVelocity = Out[1]; *ProfPosition = Out[2]; 
		return (SUCCESS);
	}
	else return (FAIL);
}
