#include <stdio.h>
#include <stdlib.h>
#include<string.h>
#include"main.h"
#include"TxModule.h"
#include "LogFile.h"

enum{CMD_SET_P, CMD_SET_IM, CMD_START_PROFILE, CMD_START_IM, CMD_STOP, CMD_GET_PROFILE, CMD_GET_MODE, CMD_GET_ERR, CMD_GET_STATUS, CMD_RESET, CMD_MAX};
char *Cmd[] = {"set profile ", "set immediate ", "start profile ", "start immediate", "stop",
				"get profile ", "get mode",	"get error",	"get status",	"reset"};

void TxModuleInit()
{	InitLogFile();
}

void ProcessTxModule(char *str)
{	int cmdNo , i;
	cmdNo = CommandResolver(str); //resolve command from string and return command no
	if((cmdNo >= CMD_SET_P) && (cmdNo <= CMD_RESET)) //if valid command 
	{	CommandAssigner(cmdNo, str);	//assign commands according to command no
		SaveLog(str);
	}
	else	//if no valid command 
	{	i = SearchTxtFile(str); //search for .txt file 
		if(i == FALSE)	printf("Warning: Invalid command\n"); 
	}
}

void CommandAssigner(int cmdNo, char *str) //assign commands according to command no
{	int val1,val2,val3;//,Ack=0;
	CMD_ACK CmdAck;
	printf("\n\n--------------Tx Module----------------\n");
	switch(cmdNo)
	{	case CMD_SET_P: val1 = ExtractValAfterSpace(str,2);	//extract value after space according to command
						val2 = ExtractValAfterSpace(str,3); 
						val3 = ExtractValAfterSpace(str,4); 
						if((val1 == INVALID_CMD) || (val2 == INVALID_CMD) || (val3 == INVALID_CMD))
						{	printf("Warning: Invalid command prameters\n");
							printf("set profile <profile number> <position in mm> <velocity in mm/s>\n");
						}	
						else
						{	printf("Setting profile number:%d\n",val1);
							CmdAck = ProcessCommand(WRITE,I_C_OBJ_PROFIL,SI_PROFILE_NO,val1); 
							if(CmdAck.Flag == TRUE) //check Ack from previous cmd
							{	printf("Setting profile velocity:%d\n",val2);
								CmdAck = ProcessCommand(WRITE,I_C_OBJ_PROFIL,SI_PROFILE_VELOCITY,val2); 
								if(CmdAck.Flag== TRUE)
								{	printf("Setting profile position:%d\n",val3);
									ProcessCommand(WRITE,I_C_OBJ_PROFIL,SI_PROFILE_POSITION,val3);
								}
							}
						}
						break;
		case CMD_SET_IM: 
						val1 = ExtractValAfterSpace(str,2);	 
						val2 = ExtractValAfterSpace(str,3);
						if((val1 == INVALID_CMD) || (val2 == INVALID_CMD))
						{	printf("Warning: Invalid command prameters\n");
							printf("set immediate <position in mm> <velocity in mm/s>\n");
						}	
						else
						{	printf("Setting immediate position:%d\n",val1);
							CmdAck = ProcessCommand(WRITE,I_C_OBJ_IMMEDIATE,SI_IMMEDIATE_POSITION,val1);
							printf("Setting immediate velocity:%d\n",val2);
							CmdAck = ProcessCommand(WRITE,I_C_OBJ_IMMEDIATE,SI_IMMEDIATE_VELOCITY,val2);														
						}
						break;
		case CMD_START_PROFILE: 
						val1 = ExtractValAfterSpace(str,2);	 
						if(val1 == INVALID_CMD)
						{	printf("Warning: Invalid command prameters\n");
							printf("start profile <profile number>\n");
						}	
						else
						{	printf("Setting Profile for run:%d\n",val1);
							CmdAck = ProcessCommand(READ,I_C_OBJ_PROFIL,SI_PROFILE_NO,val1);
							if(CmdAck.Flag == TRUE)
							{	printf("Start profile move\n");
								CmdAck = ProcessCommand(WRITE,I_C_OBJ_OP_MODE,SI_OP_MODE,D_OP_MODE_SINGLE_P_MOVE);								
							}
						}
						break;										
		case CMD_START_IM:
						printf("Start immidiate motor\n");
						CmdAck = ProcessCommand(WRITE,I_C_OBJ_OP_MODE,SI_OP_MODE,D_OP_MODE_IMMEDIATE_MOVE);
						
						break;
		case CMD_STOP:	printf("Stopping the motor\n");
						CmdAck = ProcessCommand(WRITE,I_O_OBJ_CTL,SI_CTL,D_CTL_STOP);												
						break;
		case CMD_GET_PROFILE: 
						val1 = ExtractValAfterSpace(str,2);	 
						if(val1 == INVALID_CMD)
						{	printf("Warning: Invalid command prameters\n");
							printf("get profile <profile number>\n");
						}	
						else
						{	printf("Get profile number:%d\n",val1);
							CmdAck = ProcessCommand(READ,I_C_OBJ_PROFIL,SI_PROFILE_NO,val1);
							if(CmdAck.Flag == TRUE)
							{	CmdAck = ProcessCommand(READ,I_C_OBJ_PROFIL,SI_PROFILE_VELOCITY,0);
								val2 = CmdAck.Val;
								CmdAck = ProcessCommand(READ,I_C_OBJ_PROFIL,SI_PROFILE_POSITION,0);
								val3 = CmdAck.Val;
								printf(" Velocity = %d, Position = %d \n",val1,val2,val3);	
							}							
						}
						break;
		case CMD_GET_MODE: 
						CmdAck = ProcessCommand(READ,I_C_OBJ_OP_MODE,SI_OP_MODE,0);
						switch(CmdAck.Val)
						{	case D_OP_MODE_IMMEDIATE_MOVE:	printf(">Current mode is Immediate move.\n");break;
							case D_OP_MODE_SINGLE_P_MOVE:	printf(">Current mode is Single profile move.\n");break;
							case D_OP_MODE_CYCLIC_P_MOVE:	printf(">Current mode is Cyclic profile move.\n");break;
							default: printf(">Current mode unknown.\n");break;
						} 
						break;
		case CMD_GET_ERR:
						CmdAck = ProcessCommand(READ,I_C_OBJ_ERR_CODE,SI_ERROR_SUB_IND,0);
						if(CmdAck.Val == 0) printf(">No errors.\n");
						else ShowErrWarning(val1);
						break;								
		case CMD_GET_STATUS:
						CmdAck = ProcessCommand(READ,I_O_OBJ_STATUS,SI_STATUS,0);
						switch(CmdAck.Val)
						{	case D_STATUS_MOV_ACT:	printf(">Status: Move active.\n");break;
							case D_STATUS_ERR:	printf(">Status: Error.\n");break;
							default: printf(">Status unknown.\n");break;
						}  
						break;
		case	CMD_RESET:
						CmdAck = ProcessCommand(WRITE,I_C_OBJ_ERR_CODE,SI_ERROR_SUB_IND,0); //claers error code
						if(CmdAck.Val == 0) printf(">Reset: Error code cleared\n");
						CmdAck = ProcessCommand(WRITE,I_C_OBJ_OP_MODE,SI_OP_MODE,D_OP_MODE_IMMEDIATE_MOVE); //claers error code
						if(CmdAck.Val == 0) printf(">Reset: Set to immediate mode.\n");
						break;
		default: 
							printf("Warning: Invalid command\n"); 
						break;				
	}
	printf("---------------------------------------\n\n\n");
}
 

enum{ STATE_SEND_REQ, STATE_GET_ANS, STATE_READ_STATUS, STATE_CHECK_ERR_FLG, STATE_READ_ERR_CODE,STATE_CLEAR_ERR, STATE_CLEAR_ERR_RESP};
 
CMD_ACK ProcessCommand(char Mode, unsigned int ObjectId, unsigned char SubID, unsigned int Data)
{	int state = STATE_SEND_REQ, exit = 0;
	unsigned int RxData=0,  data = 0; 
	CMD_ACK CmdAck;
	CmdAck.Val = 0; CmdAck.Flag = ERROR1;
	while(1)
	{	switch(state)
		{	case STATE_SEND_REQ:	SendCommand(Mode,ObjectId,SubID,Data); state = STATE_GET_ANS;
									break;		
			case STATE_GET_ANS:		CmdAck.Flag = TMReadResponse(&RxData);//wait for response
									if(CmdAck.Flag == TRUE) 
									{	data = RxData;
										CmdAck.Val = RxData;
										state = STATE_READ_STATUS;
									}
									else printf("*Error: CRC not matched\n");									
									break;					
			case STATE_READ_STATUS:	SendCommand(READ,I_O_OBJ_STATUS,SI_STATUS,0); state = STATE_CHECK_ERR_FLG;
									break;
			case STATE_CHECK_ERR_FLG:
									CmdAck.Flag = TMReadResponse(&RxData);//wait for response
									if(CmdAck.Flag == TRUE) 
									{	if(RxData & 1){ printf("***  Error Status  ***\n");  }
										else 
										{	exit = 1; return(CmdAck);//return(data);
										}
										state = STATE_READ_ERR_CODE;
									}
									else if(CmdAck.Flag == FALSE) printf("*Error: CRC not matched\n");									
									break;									
			case STATE_READ_ERR_CODE:
									SendCommand(READ,I_C_OBJ_ERR_CODE,SI_ERROR_SUB_IND,0); 
									state = STATE_CLEAR_ERR;									
									break;
			case STATE_CLEAR_ERR:	CmdAck.Flag = TMReadResponse(&RxData);//wait for response
									if(CmdAck.Flag == TRUE) 
									{	ShowErrWarning(RxData);
										SendCommand(WRITE,I_O_OBJ_CTL,SI_CTL,D_CTL_ERR_CLR); 
										state = STATE_CLEAR_ERR_RESP;
									}
									else if(CmdAck.Flag == FALSE) printf("*Error: CRC not matched\n");
									break;
			case STATE_CLEAR_ERR_RESP://6
									CmdAck.Flag = TMReadResponse(&RxData);//wait for response
									if(CmdAck.Flag == TRUE) 
									{	exit = 1;
										printf("***  Error cleared  ***\n");
									}
									else if(CmdAck.Flag == FALSE) printf("*Error: CRC not matched\n");
									CmdAck.Flag = ERROR1;
									return(CmdAck);//return(-1); 
			default: break;																		
		}
		if(exit)
		{	break;	
		}
	}
	CmdAck.Flag = ERROR1;
	return(CmdAck);
	//return(-1);
}

#define CMD_LEN 8
unsigned char ToMotor[CMD_LEN]; 
void SendCommand(char Mode, unsigned int ObjectId, unsigned char SubID, unsigned int Data)
{	unsigned int CkSum = 0, i = 0, j = 0;
	ToMotor[i++] = (ObjectId >> 8) & 0xFF;
	ToMotor[i++] = ObjectId & 0xFF;
	ToMotor[i++] = SubID;
	Data |= (Mode << 31);
	ToMotor[i++] = (Data >> 24) & 0xFF;
	ToMotor[i++] = (Data >> 16) & 0xFF;
	ToMotor[i++] = (Data >> 8) & 0xFF;
	ToMotor[i++] = Data & 0xFF;
	for(j=0;j<i;j++) CkSum += ToMotor[j];
	ToMotor[i] = CkSum & 0xFF;
	 
	openAPITxToRx(ToMotor,8);	//send to API here
}


int ExtractValAfterSpace(char *Str, int SpacePos)
{	unsigned int num=0, pos = 0, i = 0, maxLen = strlen(Str);
	char *lcPtr = Str; char numChar[16];
	pos = 0;	
	
	for(i = 0; i < SpacePos; i++) //get the pointer to SpacePos(on function call) space
	{	pos = pos + (char*)memchr(lcPtr,' ',strlen(lcPtr)) - lcPtr + 1;
		if(pos > maxLen) { return(INVALID_CMD); }
		for(; pos < maxLen; pos++) if(Str[pos] != ' ') {lcPtr = &Str[pos]; break;}  //to escape extra space 
	} 
	
	i = 0;
	while((lcPtr[i] != 0) && (lcPtr[i] != ' ')) //get the string after space till next space or NULL
	{	numChar[i] = lcPtr[i];  i++;
	}
	numChar[i] = 0;
	num = atoi(numChar); //convert integer to num
	return(num);
}

int SearchTxtFile(char *str)	//search .Txt file
{	int i=0,j=0,cmdNo, l1 = strlen(str), l2 = strlen(".txt");
	char cmd[100][32], c;
	int status = FALSE;
	FILE *fp;
	if(strcmp(&str[l1-4],".txt") == 0) //if .txt in it
	{	fp = fopen(str,"r"); 				
		if(fp != NULL)					//valid file name
		{	printf("File found\n"); 
			c = getc(fp);
			while(c != EOF)	//save data to array
			{	cmd[i][j++] = (char)c;
				if(c == '\n') 	//if one line scan complete
				{ 	cmd[i][j] = 0; //add null at end
					cmdNo = CommandResolver(cmd[i]); //resolve command
					if((cmdNo >= CMD_SET_P) && (cmdNo <= CMD_RESET)) //for valid command
					{	CommandAssigner(cmdNo, cmd[i]);
						j=0; i++; 
					}
					else
					{	if(cmd[i][0] != '\n') //if only new line
							printf("Invalid Command.: %s\n",cmd[i]);
						j = 0;
					}
					
				}			
				c = (char)getc(fp);	//gets single character
			}  
			status = TRUE;
		}	
		else	//if file not found
		{	printf("File not found\n");
			status = FALSE;
		}	
		fclose(fp);
	}
	return status;
} 

int CommandResolver(char *str)	//resolve command according to string input from command line or text file
{	int i, n = 0, cmdNo = -1, tmp = 0;
	for(i = 0 ; i < CMD_MAX ; i++) //get command index using string compare
	{	n = strlen(Cmd[i]);
		if(strncmp(str,Cmd[i],n) == 0)	//search for valid command
		{	if((i==CMD_START_IM) || (i==CMD_STOP) ||  (i==CMD_GET_MODE) || (i==CMD_GET_ERR) || (i==CMD_GET_STATUS) || (i==CMD_RESET) ) //confirm string for no aad-on char
			{	if((str[n] == 0) || (str[n] == '\n')) { cmdNo = i; break; } 
			}
			else //command having payload
			{	cmdNo = i; break;
			}
		} 
	}
	return  cmdNo;
}

void ShowErrWarning(int Err)	//print warnings
{	int i = 0, tmp = 0;
	switch(Err)
	{	case  D_ERR_OBJ_NOT_EXISTS: printf("***Error: object doesn�t exist***\n"); break;
		case  D_ERR_OBJ_READ_ONLY:	printf("***Error: object is read-only***\n"); break;
		case  D_ERR_OBJ_WRITE_ONLY:	printf("***Error: object write only***\n"); break;
		case  D_ERR_VALUE_TOO_HIGH:	printf("***Error: value too high***\n"); break;
		case  D_ERR_VALUE_TOO_LOW:	printf("***Error: value too low***\n"); break;
		case  D_ERR_IMMEDIATE_V_0:	printf("***Error: immediate velocity is 0***\n"); break;
		case  D_ERR_NO_PROF_CONFIG:	printf("***Error: no profiles configured***\n"); break;
		default:break;
	}
	
}

//----------------------------Response-------------------------------------
#define RX_BUF_SZ 32
unsigned char Rx2Buf[RX_BUF_SZ];
int Rx2Due = 0, Rx2Rd = 0, Rx2Wr = 0;

void FillTxModuleRxBuf(unsigned char Val) //takes value from RXmodule ot motor and fill buffer
{	Rx2Buf[Rx2Wr] = Val;
	Rx2Due = Rx2Due + 1; Rx2Wr = (Rx2Wr+1)&(RX_BUF_SZ-1);
}

 

int TMReadResponse(unsigned int *Data)	//read response from RXmodule data
{	static int state = 0;
	unsigned char data = 0;
	static unsigned int rxObjID = 0, rxSubID = 0, rxData = 0, ckSum = 0;
	//printf("TMReadCommand()\n ");
	while(Rx2Due)
	{	data = Rx2Buf[Rx2Rd];
		Rx2Due = Rx2Due - 1; Rx2Rd = (Rx2Rd+1)&(RX_BUF_SZ-1);
		
		switch(state) 
		{	case OBJ_ID_MSB_IND:	if((data == 0x10) || (data == 0x60))	//to check object index MSB as Sync byte
									{	rxObjID = data;	ckSum = data;	state++;	
									}
									break;
			case OBJ_ID_LSB_IND:	rxObjID = (rxObjID << 8) + data;ckSum += data;	state++;	break;
			case SUB_ID_IND:		rxSubID = data;					ckSum += data;	state++;	break;
			case DATA_D3_IND:		rxData = data;					ckSum += data;	state++;	break;
			case DATA_D2_IND:		rxData = (rxData << 8) + data; 	ckSum += data;	state++;	break;
			case DATA_D1_IND:		rxData = (rxData << 8) + data;	ckSum += data;	state++;	break;
			case DATA_D0_IND:		rxData = (rxData << 8) + data;	ckSum += data;	state++;	break;
			case CKSUM_IND:		state = OBJ_ID_MSB_IND;
								if(data == (ckSum & 0xFF))
								{	*Data = rxData;
									return(TRUE);									
								} 
								return(FALSE);
								//break;
			default:	state = OBJ_ID_MSB_IND;	break;					
		}
	}
	return(WAIT);
}

