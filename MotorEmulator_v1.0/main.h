#ifndef _MOTOREMU
#define _MOTOREMU

#define READ 	0
#define WRITE 	1

#define ERROR1 	-1
#define FALSE 	0
#define TRUE 	1
#define WAIT 	2


#define INVALID_CMD -2000

enum{ OBJ_ID_MSB_IND, OBJ_ID_LSB_IND, SUB_ID_IND, DATA_D3_IND, DATA_D2_IND, DATA_D1_IND, DATA_D0_IND, CKSUM_IND};

//------configration object Index-------------
#define I_C_OBJ_OP_MODE				0x1000	//Operation mode
#define I_C_OBJ_IMMEDIATE			0x1001	//Immediate 
#define I_C_OBJ_PROFIL				0x1002	// Profile
#define I_C_OBJ_ERR_CODE			0x1005	//Error code

//-- configration object subindex
//----opration mode subindex
#define SI_OP_MODE						0x00

//----Immediate Value subindex
#define SI_IMMEDIATE_VELOCITY			0x00
#define SI_IMMEDIATE_POSITION			0x01
 
//----Profile subindex
#define SI_PROFILE_NO					0x00
#define SI_PROFILE_VELOCITY				0x01
#define SI_PROFILE_POSITION				0x02

//----Error subindex
#define SI_ERROR_SUB_IND				0x00


//------opration objects--------------
#define I_O_OBJ_CTL				0x6040	//Control word 
#define I_O_OBJ_STATUS			0x6041	//Status word
#define I_O_OBJ_CURRENT			0x6060	//current

//--SUBOBJ ind
#define SI_CTL					0x00
#define SI_STATUS				0x00
#define SI_CURRENT_POS			0x00
#define SI_CURRENT_VEL			0x01

//opration mode data code
#define D_OP_MODE_IMMEDIATE_MOVE	0x00
#define D_OP_MODE_SINGLE_P_MOVE		0x01
#define D_OP_MODE_CYCLIC_P_MOVE		0x02

//---------error codes data-------------
#define D_ERR_OBJ_NOT_EXISTS		1
#define D_ERR_OBJ_READ_ONLY			2
#define D_ERR_OBJ_WRITE_ONLY		3
#define D_ERR_VALUE_TOO_HIGH		6
#define D_ERR_VALUE_TOO_LOW			7
#define D_ERR_IMMEDIATE_V_0			8
#define D_ERR_NO_PROF_CONFIG		9

//--------control word data------------
#define D_CTL_START				0x00
#define D_CTL_STOP				0x01
#define D_CTL_ERR_CLR			0x02

#define D_STATUS_MOV_ACT		0x00
#define D_STATUS_ERR			0x01

int openAPITxToRx(char *ch, int len);
int openAPIRxToTx(char *ch, int len);
void PrintStr(char *ch, int len);

#endif

