#include <stdio.h>
#include <stdlib.h>
#include "main.h"
#include "RxModule.h"
#include "TxModule.h"

int openAPITxToRx(char *ch, int len)
{	int i=0;
	for( i = 0 ; i < len ; i++)
	{	FillRxModuleRxBuf(ch[i]);
	}
//	PrintStr(ch,len);
	ProcessRxModule();
}
int openAPIRxToTx(char *ch, int len)
{	int i=0;
	for( i = 0 ; i < len ; i++)
	{	FillTxModuleRxBuf(ch[i]);
	}
	//PrintStr(ch,len);
	//ProcessTxModule();
}

void PrintStr(char *ch, int len)
{	int i;
	for(i = 0; i < len ;i++)
	{	printf("%d,",(unsigned char)ch[i]);
	}
	printf("\n");
}

int main(int argc, char* argv[]) 
{  	InitFiles();
	TxModuleInit();
	RxModuleInit();
	if( argc == 2 ) 
	{	ProcessTxModule(argv[1]);
   	}
   	else
   	{		
   		printf("Warning : Please pass a valid command\n");
	}
	PrintMotorCurrentState();
	return 0;
}
