#include <stdio.h>
#include <stdlib.h>
#include"main.h"
#include"RxModule.h"
#include"RxModuleFiles.h"

#define RX_BUF_SZ 32
RXMODULE RxMod;
unsigned char RxBuf[RX_BUF_SZ];
int RxDue = 0, RxRd = 0, RxWr = 0;

void RxModuleInit()
{	//printf("RxModuleInit()\n ");
	//fopen();
}

void FillRxModuleRxBuf(unsigned char Val)
{	RxBuf[RxWr] = Val;
	RxDue = RxDue + 1; RxWr = (RxWr+1)&(RX_BUF_SZ-1);
}

void ProcessRxModule()
{	RMReadCommand();
}

int RMReadCommand()
{	static int state = 0;
	unsigned char data = 0;
	static unsigned int rxObjID = 0, rxSubID = 0, rxData = 0, ckSum = 0;
	//printf("RMReadCommand()\n ");
	while(RxDue)
	{	data = RxBuf[RxRd]; 
		RxDue = RxDue - 1; RxRd = (RxRd+1)&(RX_BUF_SZ-1);
		
		switch(state) 
		{	case OBJ_ID_MSB_IND:	if((data == 0x10) || (data == 0x60))	//to check object index MSB as Sync byte
									{	rxObjID = data;	ckSum = data;	state++;
									}
									break;
			case OBJ_ID_LSB_IND:	rxObjID = (rxObjID << 8) + data;ckSum += data;	state++;	break;
			case SUB_ID_IND:		rxSubID = data;					ckSum += data;	state++;	break;
			case DATA_D3_IND:		rxData = data;					ckSum += data;	state++;	break;
			case DATA_D2_IND:		rxData = (rxData << 8) + data; 	ckSum += data;	state++;	break;
			case DATA_D1_IND:		rxData = (rxData << 8) + data;	ckSum += data;	state++;	break;
			case DATA_D0_IND:		rxData = (rxData << 8) + data;	ckSum += data;	state++;	break;
			case CKSUM_IND:		if(data == (ckSum & 0xFF))
								{	ProcessRxData(rxObjID, rxSubID, rxData);									
								}
								else
								{	//send cksum err
								}
								state = OBJ_ID_MSB_IND;
								return(TRUE);
								//break;
			default:	state = OBJ_ID_MSB_IND;	 break;					
		}
	}
	return(FALSE);
}

void ProcessRxData(unsigned int ObjectId, unsigned char SubID, unsigned int Data)
{	//printf("ProcessRxData()\n ");
	int ack = -1;
	int Tmp = 0;
	static int TmpProfileNo = -1;
	switch(ObjectId)
	{	case I_C_OBJ_OP_MODE:	if(SubID == 0x00)
								{	if(Data & (1 << 31)) 
									{	RxMod.OprationMode = Data & ~(1 << 31);
										switch(RxMod.OprationMode)
										{	case D_OP_MODE_IMMEDIATE_MOVE:	RxMod.CurrVelocity = RxMod.ImdVelocity; RxMod.CurrPos = RxMod.ImdPos; RxMod.Control = START; break;
											case D_OP_MODE_SINGLE_P_MOVE:	RxMod.CurrVelocity = RxMod.PrfVelocity; RxMod.CurrPos = RxMod.PrfPos; RxMod.Control = START; break;
											case D_OP_MODE_CYCLIC_P_MOVE:	break;	
										}
									}
									SendResponse(ObjectId, SubID, RxMod.OprationMode);
								}								
								else
								{	RxMod.Status |= D_STATUS_ERR;
									RxMod.Error = D_ERR_OBJ_NOT_EXISTS;
								}
								break;	
		case I_C_OBJ_IMMEDIATE:	if(SubID == SI_IMMEDIATE_VELOCITY)
								{	if(Data & (1 << 31))
									{	Tmp = Data & ~(1 << 31);
										if(Tmp >= (1 <<  30)) Tmp -= (1 << 31);  //for negetive value										
										if((Tmp > MIN_VELOCITY) && (Tmp <= MAX_VELOCITY)) {RxMod.ImdVelocity = Tmp;}
										else if(Tmp < MIN_VELOCITY) { RxMod.Status |= D_STATUS_ERR; RxMod.Error = D_ERR_VALUE_TOO_LOW; }
										else if(Tmp > MAX_VELOCITY) { RxMod.Status |= D_STATUS_ERR; RxMod.Error = D_ERR_VALUE_TOO_HIGH; }
										
										if(Tmp == 0)
										{	RxMod.Status |= D_STATUS_ERR; 
											RxMod.Error = D_ERR_IMMEDIATE_V_0;
										}
									}
									SendResponse(ObjectId, SubID, RxMod.ImdVelocity);
								}
								else if(SubID == SI_IMMEDIATE_POSITION)
								{	if(Data & (1 << 31))
									{	Tmp = Data & ~(1 << 31);
										if(Tmp >= (1 <<  30)) Tmp -= (1 << 31);  //for negetive value
										if((Tmp >= MIN_POSITION) && (Tmp <= MAX_POSITION)) {RxMod.ImdPos = Tmp;}
										else if(Tmp < MIN_POSITION) { RxMod.Status |= D_STATUS_ERR; RxMod.Error = D_ERR_VALUE_TOO_LOW; }
										else if(Tmp > MAX_POSITION) { RxMod.Status |= D_STATUS_ERR; RxMod.Error = D_ERR_VALUE_TOO_HIGH; }										
									}
									SendResponse(ObjectId, SubID, RxMod.ImdPos);
								}
																
								else
								{	RxMod.Status |= D_STATUS_ERR;
									RxMod.Error = D_ERR_OBJ_NOT_EXISTS;
								}
								break;
		case I_C_OBJ_PROFIL:	if(SubID == SI_PROFILE_NO)
								{	Tmp = Data & ~(1 << 31);
									if(Data & (1 << 31))
									{	if(Tmp >= MAX_PROFILE) { RxMod.Status |= D_STATUS_ERR; RxMod.Error = D_ERR_NO_PROF_CONFIG; }										
										else
										{	RxMod.PrfNo	= Tmp;
										}
										TmpProfileNo = Tmp;
									}
									else
									{	if(Tmp >= MAX_PROFILE) { RxMod.Status |= D_STATUS_ERR; RxMod.Error = D_ERR_NO_PROF_CONFIG; }										
										else
										{	RxMod.PrfNo	= Tmp;
											ack = ReadProfile(RxMod.PrfNo,&RxMod.PrfVelocity,&RxMod.PrfPos);
											
											if((ack == PROFILE_NA) || (ack == FAIL))
											{	RxMod.Status |= D_STATUS_ERR; RxMod.Error = D_ERR_NO_PROF_CONFIG;
											}
											else
											{	//RxMod.CurrVelocity = RxMod.PrfVelocity; RxMod.CurrPos = RxMod.PrfPos;
											}	
										}
									}
									SendResponse(ObjectId, SubID, RxMod.PrfNo);
								}
								else if(SubID == SI_PROFILE_VELOCITY)
								{	if(Data & (1 << 31))
									{	Tmp = Data & ~(1 << 31);
										if(Tmp >= (1 <<  30)) Tmp -= (1 << 31);  //for negetive value										
										if((Tmp >= MIN_VELOCITY) && (Tmp <= MAX_VELOCITY)) {RxMod.PrfVelocity = Tmp;}
										else if(Tmp < MIN_VELOCITY) { RxMod.Status |= D_STATUS_ERR; RxMod.Error = D_ERR_VALUE_TOO_LOW; }
										else if(Tmp > MAX_VELOCITY) { RxMod.Status |= D_STATUS_ERR; RxMod.Error = D_ERR_VALUE_TOO_HIGH; }
									}
									SendResponse(ObjectId, SubID, RxMod.PrfVelocity);
								}
								else if(SubID == SI_PROFILE_POSITION)
								{	if(Data & (1 << 31))
									{	Tmp = Data & ~(1 << 31);
										if(Tmp >= (1 <<  30)) Tmp -= (1 << 31);  //for negetive value
										if((Tmp >= MIN_POSITION) && (Tmp <= MAX_POSITION)) {RxMod.PrfPos = Tmp;}
										else if(Tmp < MIN_POSITION) { RxMod.Status |= D_STATUS_ERR; RxMod.Error = D_ERR_VALUE_TOO_LOW; }
										else if(Tmp > MAX_POSITION) { RxMod.Status |= D_STATUS_ERR; RxMod.Error = D_ERR_VALUE_TOO_HIGH; }
										
										if(TmpProfileNo < MAX_PROFILE) 
											SaveProfile(RxMod.PrfNo,RxMod.PrfVelocity,RxMod.PrfPos);
										 
									}
									SendResponse(ObjectId, SubID, RxMod.PrfPos);
								}
								else
								{	RxMod.Status |= D_STATUS_ERR;
									RxMod.Error = D_ERR_OBJ_NOT_EXISTS;
								}
								break;
		case I_C_OBJ_ERR_CODE:	if(SubID == 0x00)
								{	if(Data & 1)
									{	RxMod.Status |= D_STATUS_ERR;
										RxMod.Error = D_ERR_OBJ_READ_ONLY;
									}
									else
									{	SendResponse(ObjectId, SubID, RxMod.Error);
									}
								}
								else
								{	RxMod.Status |= D_STATUS_ERR;
									RxMod.Error = D_ERR_OBJ_NOT_EXISTS;
								}
								//after sending
								RxMod.Error = 0;
								break;
		case I_O_OBJ_CTL:		if(SubID == SI_CTL)
								{	switch(Data & 3)
									{	case D_CTL_START: 	RxMod.Control = START;  break;
										case D_CTL_STOP: 	RxMod.Control = STOP;  break;
										case D_CTL_ERR_CLR: RxMod.Error = 0; break;
										default:break;
									}
									SendResponse(ObjectId, SubID, RxMod.Control);									
								}
								else
								{	RxMod.Status |= D_STATUS_ERR;
									RxMod.Error = D_ERR_OBJ_NOT_EXISTS;
								}
								break;
		case I_O_OBJ_STATUS:	if(SubID == 0x00)
								{	if(Data & 1)
									{	RxMod.Status |= D_STATUS_ERR;
										RxMod.Error = D_ERR_OBJ_READ_ONLY;
									}
									else
									{	if(RxMod.Error == 0) RxMod.Status = 0;
										SendResponse(ObjectId, SubID, RxMod.Status);
									}
								}
								else
								{	RxMod.Status |= D_STATUS_ERR;
									RxMod.Error = D_ERR_OBJ_NOT_EXISTS;
								}
								break;
		case I_O_OBJ_CURRENT:
								break;
		default:
								break;																														
	}
	SaveCurrent();	
}

void SendResponse(unsigned int ObjectId, unsigned char SubID, unsigned int Data)
{	unsigned char respBuf[8];
	unsigned int ckSum = 0, i = 0, j = 0;
	respBuf[i++] = (ObjectId >> 8) & 0xFF;
	respBuf[i++] = ObjectId & 0xFF;
	respBuf[i++] = SubID;
	respBuf[i++] = (Data >> 24) & 0xFF;
	respBuf[i++] = (Data >> 16) & 0xFF;
	respBuf[i++] = (Data >> 8) & 0xFF;
	respBuf[i++] = Data & 0xFF;
	for(j=0;j<i;j++) ckSum += respBuf[j];
	respBuf[i] = ckSum & 0xFF;
	
	//send API here  
	openAPIRxToTx(respBuf,8);
}


void PrintMotorCurrentState()
{	printf("------------------Rx Module----------------\n");
	switch(RxMod.OprationMode)
	{	case D_OP_MODE_IMMEDIATE_MOVE:	printf("Rx:Opration Mode: Immediate move.\n"); break;
		case D_OP_MODE_SINGLE_P_MOVE:	printf("Rx:Opration Mode: Single Profile move.\n"); break;
		case D_OP_MODE_CYCLIC_P_MOVE:	printf("Rx:Opration Mode: Cyclic Profile move.\n"); break;
	}
	printf("Rx:Current velocity: %d, Current Position: %d\n",RxMod.CurrVelocity,RxMod.CurrPos);
	if(RxMod.Control == START) printf("Rx:Motor Status: RUNNING\n");
	else printf("Rx:Motor Status: STOPPED\n");
	printf("-------------------------------------------\n\n");
}
